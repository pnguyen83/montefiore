/**
* @description: if a Unavailable availability record of resources is created/updated and approved, all jobs of those resources on the days
* of Availability records, will be set to Pending Allocation (with jobs have one resource) and don't change if jobs have many resources
*/
public with sharing class skedAvailabilityHandler {
	public static void afterInsert(List<sked__Availability__c> newAvailabilities) {
        updateJobInUnavaiPeriod(newAvailabilities, null);
    }

    public static void afterUpdate(List<sked__Availability__c> newAvailabilities,
                                    Map<id, sked__Availability__c> map_old_id_availability) {
        updateJobInUnavaiPeriod(newAvailabilities, map_old_id_availability);
    }

    //===================================================Private Functions========================================//
    public static void updateJobInUnavaiPeriod(List<sked__Availability__c> newAvailabilities,
                                                    Map<id, sked__Availability__c> map_old_id_availability) {
        Map<String, List<sked__Availability__c>> map_resId_unAvailabilities = new Map<String, List<sked__Availability__c>>();
        Set<Date> unAvailableDates = new Set<Date>();
        List<sked__Availability__c> recordsToBeProcessed = new List<sked__Availability__c>();

        //get list of availabilities to be processed
        for (sked__Availability__c record : newAvailabilities) {
            if (record.sked__Status__c == skedConstants.AVAILABILITY_STATUS_APPROVED && record.sked__Is_Available__c == false) {
                recordsToBeProcessed.add(record);
            }
        }

        if (!recordsToBeProcessed.isEmpty()) {
            //get unavailable dates of resources
            unAvailableDates = getDatesFromUnAvailableRecords(recordsToBeProcessed);

            //get List unavailable records of each resource
            map_resId_unAvailabilities = getListUnAvailabilitiesEachResource(recordsToBeProcessed);

            //check and update related job allocations of each resource
            checkAndUpdateJobAllocations(unAvailableDates, map_resId_unAvailabilities);
        }
    }

    private static Map<String, List<sked__Availability__c>> getListUnAvailabilitiesEachResource (List<sked__Availability__c> skedUnAvailabilities) {
        Map<String, List<sked__Availability__c>> map_resId_unAvailabilities = new Map<String, List<sked__Availability__c>>();

        for (sked__Availability__c record : skedUnAvailabilities) {
            List<sked__Availability__c> availabilityList = map_resId_unAvailabilities.get(record.sked__Resource__c);

            if (availabilityList == null) {
                availabilityList = new List<sked__Availability__c>();
            }
            availabilityList.add(record);
            map_resId_unAvailabilities.put(record.sked__Resource__c, availabilityList);
        }

        return map_resId_unAvailabilities;
    }

    private static Set<Date> getDatesFromUnAvailableRecords (List<sked__Availability__c> skedUnAvailabilities) {
        Set<Date> unAvailableDates = new Set<Date>();

        for (sked__Availability__c record : skedUnAvailabilities) {
            Date recordStartDate = skedDateTimeUtils.getDate(record.sked__Start__c, record.sked__Timezone__c);
            Date recordFinishDate = skedDateTimeUtils.getDate(record.sked__Finish__c, record.sked__Timezone__c);

            while (recordStartDate <= recordFinishDate) {
                unAvailableDates.add(recordStartDate);
                System.debug('recordStartDate ' + recordStartDate);
                recordStartDate = recordStartDate.addDays(1);
            }
        }

        return unAvailableDates;
    }

    private static void checkAndUpdateJobAllocations (Set<Date> unAvailableDates,
                                                        Map<String, List<sked__Availability__c>> map_resId_unAvailabilities) {
        List<sked__Job_Allocation__c> jasToBeUpdated = new List<sked__Job_Allocation__c>();

        for (sked__Job_Allocation__c skedJA : [SELECT id, Name, sked__resource__c, sked__Job__c, sked__Job__r.sked__Start__c,
                                                    sked__Job__r.sked__Finish__c, sked__Job__r.Name
                                                    FROM sked__Job_Allocation__c
                                                    WHERE sked__resource__c IN :map_resId_unAvailabilities.keySet()
                                                    AND (DAY_ONLY(convertTimezone(sked__Job__r.sked__Start__c)) IN :unAvailableDates
                                                            OR DAY_ONLY(convertTimezone(sked__Job__r.sked__Finish__c)) IN :unAvailableDates)
                                                    AND sked__Status__c != :skedConstants.JA_STATUS_DECLINED
                                                    AND sked__Status__c != :skedConstants.JA_STATUS_COMPLETE
                                                    AND sked__Status__c != :skedConstants.JA_STATUS_DELETED
                                                    ]) {
            System.debug('1 Job Name ' + skedJA.sked__Job__r.Name);
            if (map_resId_unAvailabilities.containsKey(skedJA.sked__resource__c)) {
                List<sked__Availability__c> skedUnAvailabilities = map_resId_unAvailabilities.get(skedJA.sked__resource__c);
                System.debug('2 Job Name ' + skedJA.sked__Job__r.Name);
                for (sked__Availability__c record : skedUnAvailabilities) {
                    if (skedJA.sked__Job__r.sked__Start__c < record.sked__Finish__c
                        && skedJA.sked__Job__r.sked__Finish__c > record.sked__Start__c) {
                        skedJA.sked__Status__c = skedConstants.JA_STATUS_DELETED;
                        jasToBeUpdated.add(skedJA);
                    }
                }
            }
        }

        if (!jasToBeUpdated.isEmpty()) {
            update jasToBeUpdated;
        }
    }
}
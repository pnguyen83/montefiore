public with sharing class skedDateTimeUtils {
    public static final string DATE_ISO_FORMAT = 'yyyy-MM-dd';

	public static Date getDate(DateTime input, string timezoneSidId) {
        string dateIsoString = input.format(DATE_ISO_FORMAT, timezoneSidId);
        return (Date)Json.deserialize('"' + dateIsoString + '"', Date.class);
    }
}